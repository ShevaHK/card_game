import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Michail on 11/20/2019.
 */
public class Main {
    public static void main(String[] args) {
        ArrayList<Card> list = new ArrayList<>();
        list.add(new Card("cherva", "king", 7, false));
        list.add(new Card("bubna", "dama", 6, true));
        list.add(new Card("cherva", "valet", 5, false));
        list.add(new Card("trefa", "ten", 4, false));
        list.add(new Card("cherva", "dama", 6, false));
        list.add(new Card("trefa", "ace", 8, false));
        list.add(new Card("bubna", "eight", 3, true));

        System.out.println(list);
        PlayerHand playerHand = new PlayerHand(list);
        list = playerHand.sortCard();
    //    playerHand.sortCardByWeight(list);
        System.out.println(list);

    }
}
