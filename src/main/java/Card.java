public class Card  {
    String Suit;
    String CardName;
    int CardValue;
    boolean Trump;

    public Card(String suit, String cardName, int cardValue, boolean trump) {
        if(cardValue > 1 || cardValue < 9){
            Suit = suit;
            CardName = cardName;
            CardValue = cardValue;
            Trump = trump;
        }else{
            return;
        }
    }
    public Card() {
        Suit = "no suit";
        CardName = "no name";
        CardValue = 1;
        Trump = false;
    }


    public String getSuit() {
        return Suit;
    }

    public void setSuit(String suit) {
        Suit = suit;
    }

    public String getCardName() {
        return CardName;
    }

    public void setCardName(String cardName) {
        CardName = cardName;
    }

    public int getCardValue() {
        return CardValue;
    }

    public void setCardValue(int cardValue) {
        CardValue = cardValue;
    }

    public boolean isTrump() {
        return Trump;
    }

    public void setTrump(boolean trump) {
        Trump = trump;}

    @Override
    public String toString() {
        return "Card{" +
                "Suit='" + Suit + '\'' +
                ", CardName='" + CardName + '\'' +
                ", CardValue=" + CardValue +
                ", Trump=" + Trump +
                '}'+"\n";
    }
}
