import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michail on 11/19/2019.
 */
public class PlayerHand {
    boolean move;
    ArrayList<Card> cardList;

    public PlayerHand(ArrayList<Card> cardList) {
        this.cardList = cardList;
    }


    private void sortCardBySuit(List<Card> list) {
        for (int j = 0; j < list.size(); j++) {
            Card tempCard = list.get(j);
            for (int i = j; i < list.size(); i++) {
                if (tempCard.CardValue > list.get(i).CardValue) {
                    tempCard = list.get(i);
                    list.add(j, tempCard);
                    list.remove(i + 1);

                }
            }
        }
    }

    public ArrayList<Card> sortCard() {
        ArrayList<Card> sortedList = new ArrayList<>();
        List<Card> pika = new ArrayList<>();
        List<Card> cherva = new ArrayList<>();
        List<Card> bubna = new ArrayList<>();
        List<Card> trefa = new ArrayList<>();
        for (Card card : cardList) {
            switch (card.Suit) {
                case "pika":
                    pika.add(card);
                    break;
                case "cherva":
                    cherva.add(card);
                    break;
                case "bubna":
                    bubna.add(card);
                    break;
                case "trefa":
                    trefa.add(card);
                    break;
            }
        }
        sortCardBySuit(pika);
        sortCardBySuit(cherva);
        sortCardBySuit(bubna);
        sortCardBySuit(trefa);

        sortedList.addAll(trefa);
        sortedList.addAll(bubna);
        sortedList.addAll(cherva);
        sortedList.addAll(pika);

        return sortedList;
    }

    public void sortCardByWeight(List<Card> list) {
        for (int j = 0; j < list.size(); j++) {
            Card tempCard = list.get(j);
            for (int i = j; i < list.size(); i++) {
                if (tempCard.CardValue > list.get(i).CardValue) {
                    tempCard = list.get(i);
                    list.add(j, tempCard);
                    list.remove(i + 1);

                }
            }
        }
    }

    public Card moveTheCard(int index) {
        Card card;
        card = cardList.get(index);
        return card;
    }

    public Card throwCard(int index) {
        return moveTheCard(index);
    }


}
